package main

import (
	"fmt"
	"net/http"

	"gitee.com/go-course/go7/script_exporter/controller"
	"github.com/infraboard/mcube/logger/zap"
)

// e:\Projects\Golang\go-course-projects\go7\go7\script_exporter\modules\test.py
func ScriptHandler(w http.ResponseWriter, r *http.Request) {
	sc := controller.NewScriptCollector("modules")
	qs := r.URL.Query()
	err := sc.Exec(qs.Get("module"), qs.Get("params"), w)
	if err != nil {
		fmt.Fprintf(w, fmt.Sprintf("exec error, %s", err))
	}
}

func main() {
	zap.DevelopmentSetup()
	http.HandleFunc("/metrics", ScriptHandler)
	http.ListenAndServe(":8050", nil)
}
