package collector_test

import (
	"bytes"
	"testing"

	"gitee.com/go-course/go7/script_exporter/modules/rocketmq/collector"
	"gitee.com/go-course/go7/script_exporter/modules/rocketmq/conf"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
)

var (
	// 采用新的注册表
	retgistry = prometheus.NewRegistry()
)

func TestParseLine(t *testing.T) {
	m := collector.ParseLine(`rocket_to_kafka                   8       V3_2_6_SNAPSHOT          PUSH   CLUSTERING      0        0`)
	t.Log(m)
}

func TestCollect(t *testing.T) {
	// 如果打印采集后的结果
	c := collector.NewCollector()

	// 把写好的采集器注册到当前的注册表
	if err := retgistry.Register(c); err != nil {
		t.Fatal(err)
	}

	// 通过Gather方法触发采集
	mf, err := retgistry.Gather()
	if err != nil {
		t.Fatal(err)
	}

	// 如何打印我们mf
	// 编码输出
	b := bytes.NewBuffer([]byte{})
	enc := expfmt.NewEncoder(b, expfmt.FmtText)
	for i := range mf {
		enc.Encode(mf[i])
	}

	// buffer里面就是编码后的数据
	t.Log(b.String())
}

func init() {
	conf.LoadConfigFromEnv()
	conf.C().FileConfig.Path = "sample/data.txt"
}
