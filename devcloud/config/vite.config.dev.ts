import { mergeConfig } from 'vite';
import eslint from 'vite-plugin-eslint';
import baseConfig from './vite.config.base';

// * module.exports = {
//   *   proxy: {
//   *     // string shorthand
//   *     '/foo': 'http://localhost:4567/foo',
//   *     // with options
//   *     '/api': {
//   *       target: 'http://jsonplaceholder.typicode.com',
//   *       changeOrigin: true,
//   *       rewrite: path => path.replace(/^\/api/, '')
//   *     }
//   *   }
//   * }
//   *
export default mergeConfig(
  {
    mode: 'development',
    server: {
      open: true,
      fs: {
        strict: true,
      },
      proxy: {
        '/keyauth-g7/api/v1': {
          target: 'http://localhost:8050',
          changeOrigin: true,
        },
        '/cmdb-g7/api/v1': {
          target: 'http://localhost:8060',
          changeOrigin: true,
        },
      },
    },
    plugins: [
      eslint({
        cache: false,
        include: ['src/**/*.ts', 'src/**/*.tsx', 'src/**/*.vue'],
        exclude: ['node_modules'],
      }),
    ],
  },
  baseConfig
);
