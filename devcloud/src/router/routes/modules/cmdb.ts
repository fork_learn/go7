import { DEFAULT_LAYOUT } from '@/router/constants';
import { AppRouteRecordRaw } from '../types';

const CMDB: AppRouteRecordRaw = {
  path: '/cmdb',
  name: 'cmdb',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: 'menu.cmdb.secret',
    requiresAuth: true,
    icon: 'icon-list',
    order: 0,
  },
  children: [
    {
      path: 'list_secret', // The midline path complies with SEO specifications
      name: 'ListSecret',
      component: () => import('@/views/cmdb/secret/index.vue'),
      meta: {
        locale: 'menu.cmdb.secret.list',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'detail_secret', // The midline path complies with SEO specifications
      name: 'DetailSecret',
      component: () => import('@/views/cmdb/secret/detail.vue'),
      meta: {
        locale: 'menu.cmdb.secret.detail',
        requiresAuth: true,
        roles: ['*'],
        hideInMenu: true,
      },
    },
  ],
};

export default CMDB;
