import axios from 'axios';

// 和我们定义golang strcut
export interface QuerySecret {
  page_size: number;
  page_number: number;
  //   ?表示该参数是可选的
  keywords?: string;
}

// js 版本
// export function login(data) {
//   return axios.post('/api/user/login', data);
// }

export interface SecretData {
  description: string;
  vendor: string;
  crendential_type: string;
  address: string;
  api_key: string;
  api_secret: string;
  request_rate: number;
  allow_regions: string[];
}

export interface Secret {
  id: string;
  create_at: number;
  data: SecretData;
}

// ts interface 有点像struct
export interface SecretSet {
  total: number;
  items: Secret[];
}

export interface CreateSecretRequest {
  description: string;
  vendor: string;
  crendential_type: string;
  address: string;
  api_key: string;
  api_secret: string;
  request_rate: number;
  allow_regions: string[];
}

export function QUERY_SECRET(req: QuerySecret) {
  return axios.get<SecretSet>('/cmdb-g7/api/v1/secret', {
    params: req,
  });
}

export function DESCRIBE_SECRET(secretId: string) {
  return axios.get<Secret>(`/cmdb-g7/api/v1/secret/${secretId}`);
}

export function CREATE_SECRET(req: CreateSecretRequest) {
  return axios.post<Secret>('/cmdb-g7/api/v1/secret', req);
}

export function DELETE_SECRET(secretId: string) {
  return axios.delete<Secret>(`/cmdb-g7/api/v1/secret/${secretId}`);
}
