<template>
  <div class="about">
    <!-- 通过 v-bind 绑定属性 -->
    <!-- <h1 v-bind:id="name">{{ name }}</h1> -->
    <!-- <h1 :id="name">{{ name }}</h1> -->

    <!-- 事件绑定 -->
    <!-- <button onclick="handleClick">事件绑定测试</button> -->
    <!--元素Dom的事件都是on打头, onclick -- on(v-on) + click(指令参数)-->
    <!-- @ ---- v-on: -->
    <!-- <button
      @click="handleClick"
      :style="{ color: activeColor, fontSize: fontSize }"
    >
      事件绑定测试
    </button> -->

    <!-- 表单绑定 -->
    <!-- <span>请输入你名字: </span>
    <input
      v-model="inputContent"
      v-on:keyup.enter="handleInputComplete"
      type="text"
      v-focus
      v-show="enabled"
    /> -->

    <!-- 动态生成li元素: v-for -->
    <!-- <div v-for="item in books" :key="item.name">
      <li v-if="item.name !== 'Python'">{{ item.name }}</li>
    </div> -->
  </div>
</template>
<script setup>
import { reactive, ref } from "vue";

const inputContent = ref("");
const enabled = ref(true);
const books = reactive([
  { name: "Go语言", author: "Google" },
  { name: "Python", author: "G" },
]);

const handleInputComplete = () => {
  alert(`你的输入值为: ${inputContent.value}`);
};

// const activeColor = ref("red");
// const fontSize = ref("22px");

// const name = ref('hello');
// const handleClick = () => {
//   alert("点击了我");
// };
</script>

<style>
@media (min-width: 1024px) {
  .about {
    min-height: 100vh;
    display: flex;
    align-items: center;
  }
}

.color {
  color: red;
}
.f12 {
  font-size: 22px;
}
</style>

<!-- 选项式API -->
<!-- <script>
export default {
  // 返回视图需要的展示的响应式数据, 他就是Model
  data() {
    return {
      name: "老喻",
    };
  },
  // 定义视图需要调用的函数
  methods: {
    inputChanged() {
      // data里面定义的属性, 都可以通过this访问到
      console.log(this.name);
    },
  },
  // 生命周期钩子会在组件生命周期的各个不同阶段被调用
  // 例如这个函数就会在组件挂载完成后被调用
  mounted() {
    // 等待虚拟Dom生成好了后,操作他
    document.getElementById("input").focus()
    console.log(this);
  },
};
</script> -->

<!-- 组合式API
<script setup>
// 定义一个响应式的数据, 这个变量能和 模板里面的数据进行映射, vue提供功能
// 使用reactive来构造一个Proxy(就是是一个响应式)
// 使用ref来为基础类型 构造响应式变量
// let person = {
//   name: $ref("张三"),
//   profile: $ref({ city: "北京" }),
//   skills: $ref(["Golang", "Vue"]),
// };

// import { computed, onUnmounted, watch } from "vue";

// // 解构赋值的语法, 这个解构出来的变量是不是响应式的
// // ojbject.name = xx  ==> ojbject.set(obj, name, xxx)
// // let {name, profile, skills } = person

// // 因为这些属性本身就是响应式的
// let profile = $ref({ city: "北京" });
// let skills = $ref(["Golang", "Vue"]);

// watch 跟着响应式数据变化
// 第一个参数: 响应式数据
// 第二个参数: 回调函数
// 第三个参数: watch参数
//    immediate: 数据初始化的时候 立即执行一次
// watch(
//   skills,
//   (newValue, oldValue) => {
//     console.log("new: " + newValue);
//     console.log("old: " + oldValue);
//   },
//   { immediate: true }
// );

// const skillCountTmp = skills.length;

// compute
// 直接定义一个getter方法
// let skillCount = computed({
//   get() {
//     return skillCountTmp
//   },
//   set() {

//   }
// });


// let person = ref({
//   name: "张三",
//   profile: { city: "北京" },
//   skills: ["Golang", "Vue"],
// });

// let skill = $ref("");


// onUnmounted(() => {
//   console.log("onUnmounted");
// });

// let inputComplete = () => {
//   skills.push(skill);
//   profile.skills_total = skills.length;
// };

// let name = ref("老喻");
// 注意, 要修该ref的变量, 必须使用 name.value

// onMounted(() => {
//   // 组合式API如何获取到当前vue实例
//   // let vueIns = getCurrentInstance();
//   // console.log(vueIns);

//   // 修改了响应式变量name的值, 但是更新是异步的
//   name.value = "张三";

//   // 立即通过Dom过去视图上的值() "老喻"
//   // console.log(document.getElementById("input").innerText);

//   // 需要等待vue更新周期到来后 才去获取Dom, nextTick就是这样的钩子函数
//   // nextTick(() => {
//   //   console.log(document.getElementById("input").innerText);
//   // });
// });

// HTML 元素属性@ 代表监听该元素的事件变化, 有变化后会回调我们定义的函数
// 如果让模板可以调用我们的函数
// let inputChanged = () => {
//   //  我们打印下声明的变量有没有跟着变化(响应式)
//   console.log(name.value);
// };

// onBeforeMount(() => {
//   console.log("before mount");
// });
// onMounted(() => {
//   console.log("mounted");
// });
// onBeforeUpdate(() => {
//   console.log("before update");
// });
// onUpdated(() => {
//   console.log("on updated");
// });
// onBeforeUnmount(() => {
//   console.log("before unmount");
// });
// onUnmounted(() => {
//   console.log("unmounted");
// });
</script> -->
