import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";
import ButtonCounter from "@/components/ButtonCounter.vue";

const app = createApp(App);

// 补充UI插件
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
app.use(ElementPlus);
import ArcoVue from "@arco-design/web-vue";
import "@arco-design/web-vue/dist/arco.css";
app.use(ArcoVue);

app.use(createPinia());
app.use(router);
app.component("ButtonCounter", ButtonCounter);

app.mount("#app");
