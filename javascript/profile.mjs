// export var firstName = 'Michael';
// export var lastName = 'Jackson';
// export var year = 1958;

//  export 只能暴露一个对象
// {firstName, lastName, year} => {firstName:firstName, lastName:lastName, year:year}
// export {firstName, lastName, year}


var firstName = 'Michael';
var lastName = 'Jackson';
var year = 1958;

// 封装成一个对象队伍暴露
// export var MyApp = {firstName, lastName, year};

// 帮我们固化了一个默认default的空间
// export var defaut = {firstName, lastName, year}
export default {firstName, lastName, year}